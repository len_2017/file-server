#说明
这是一个简单的读取绝对路径下的文件的服务
这里主要是读取一下路径:
```properties
server.port=8070
#需要覆盖原来默认的静态资源配置
spring.resources.static-locations[0]=classpath:/META-INF/resources/,classpath:/resources/,classpath:/static/,classpath:/public/
#添加外部文件映射
spring.resources.static-locations[1]=file:/opt/file
```
这样在file下放置date.jpg,可以访问:<http://123.56.81.120:8070/file/date.jpg> 直接查看图片
